<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<?php


?>
<div class="product-grid" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
        <?php 
        
            while ( have_posts() ): the_post(); 
            $postid = get_the_ID();
            $familysku = get_post_meta($postid, 'collection', true);
            $flooringtype = get_post_type( get_the_ID() ); //'carpeting';
            
            $args = array(
                'post_type'      => $flooringtype,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => 'collection',
                        'value'   => $familysku,
                        'compare' => '='
                    )
                )
            );

            $the_query = new WP_Query( $args );
        ?>
        <div class="col-md-6 col-sm-6">
            <div class="productName">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>
            <div class="productColor"><?php  echo $the_query->found_posts; ?> COLORS</div>
            <div class="productImage">
            <a href="<?php the_permalink(); ?>">
                <?php
                $product_swatch = get_field('swatch_image_link');

                $image = swatch_image_product_thumbnail(get_the_ID(),'514','514');

                if($product_swatch != '' )
                    { 
                        if(get_field('brand')=='Add Floors'){
                   ?>
                            <img src="<?php echo $image; ?>">
                    <?php
                        }else{
                    ?>
                    <img src="<?php echo $image; ?>">
                    <?php
                        }
                    }
                else
                {
                    ?>
                    <img src="https://placehold.it/514x514?text=NO+IMAGE">
                    <?php
                }
                ?>
            </a>    
            </div>    
            <div class="productBrand"><?php echo get_field('brand');?></div>
            <div class="likeIcon">
                <a href="#"><img src="/wp-content/uploads/2019/06/like-icon.jpg" /></a>
            </div>
            <div class="fl-module fl-module-button fl-node-5ad83d9354a3d" data-node="5ad83d9354a3d">
                <div class="fl-module-content fl-node-content">
                    <div class="fl-button-wrap fl-button-width-auto fl-button-center" style="width: 76%;margin-top: 20px;display: inline-block;">
                        <a class="fl-button" role="button" href="/500-off-flooring/" target="_self">
                            <span class="fl-button-text" style="font-size:24px;">SAVE <strong>$500</strong></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            endwhile;   
        ?>
    </div>
</div>