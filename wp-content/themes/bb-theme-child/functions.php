<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

add_action( 'wp_enqueue_scripts', function(){
wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/resources/slick/script.js","","",1);
wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
});

// Classes
require_once 'classes/fl-class-child-theme.php';



add_action( 'after_setup_theme',     'FLChildTheme::setup' );
add_action( 'init',                  'FLChildTheme::init_woocommerce' );
add_action( 'wp_enqueue_scripts',    'FLChildTheme::enqueue_scripts', 999 );
add_action( 'widgets_init',          'FLChildTheme::widgets_init' );
add_action( 'wp_footer',             'FLChildTheme::go_to_top' );

// Theme Filters
add_filter( 'body_class',            'FLChildTheme::body_class' );
add_filter( 'excerpt_more',          'FLChildTheme::excerpt_more' );

// Actions
//add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 ); 

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// add_filter('show_admin_bar', '__return_false');

//Check image path for swatch and gallery image

function get_image_url ($image,$height,$width){

  if(get_option('cloudinary',true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false || strpos($image, 's7d4.scene7.com') !== false || strpos($image, 's7d2.scene7.com') !== false || strpos($image, 's7d5.scene7.com') !== false){

      
            if(strpos($image , 's7.shawimg.com') !== false){

              $image = preg_replace('/^https(?=:\/\/)/i','http',$image);     	
                $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";

            }else{

                  if(strpos($image , 'http') === false){ 

                      $image = "https://" . $image;
                  }	
                  $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";
          }
  
    }else{
      
  
      $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;
  
    }	

    return $image;

}

//Single gallery image of product

function single_gallery_image_product($post_id,$height,$width){


  if(get_field('gallery_room_images', $post_id)){			
      
      // loop through the rows of data
      
      $gallery_image = get_field('gallery_room_images', $post_id);

  $gallery_i = explode("|",$gallery_image);
  

    foreach($gallery_i as  $key=>$value) {	
              
              
              $room_image =  get_image_url($value,'1000','1600');
             
              break;
    }
    
  }

  return $room_image;
}


//Cloudinary Swatch image high

function swatch_image_product($post_id,$height,$width){

$image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 
return  get_image_url($image,$height,$width);

}

//Cloudinary Swatch image thumbnail 
function swatch_image_product_thumbnail($post_id,$height){


  $image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 
  
  return  get_image_url($image,'514','514');    
  
  }

//Cloudinary high resolution gallery image 
function high_gallery_images_in_loop($value){

  return  get_image_url($value,'1000','1600');  

}

//Cloudinary thumnail gallery image 
function thumb_gallery_images_in_loop($value){

  return  get_image_url($value,'222','222');    

}


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}