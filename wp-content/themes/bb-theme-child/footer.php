<?php do_action('fl_content_close'); ?>

</div>
<!-- .fl-page-content -->
<?php

	do_action('fl_after_content');

	if ( FLTheme::has_footer() ) :

	?>
<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
	<?php

		do_action('fl_footer_wrap_open');
		do_action('fl_before_footer_widgets');

		FLTheme::footer_widgets();

		do_action('fl_after_footer_widgets');
		do_action('fl_before_footer');

		FLTheme::footer();

		do_action('fl_after_footer');
		do_action('fl_footer_wrap_close');

		?>
</footer>
<?php endif; ?>
<?php do_action('fl_page_close'); ?>
</div>
<!-- .fl-page -->
<?php 
	
wp_footer(); 

do_action('fl_body_close');

FLTheme::footer_code();

?>
<script>
	jQuery('.navbar-nav .menu-item-has-children').mouseover(function(){
		jQuery('header.fl-page-header-primary').after('<div class="navHover"></div>');
	});

	jQuery('.navbar-nav .menu-item-has-children').mouseout(function(){
		jQuery('.navHover').remove();
	});
</script>

<?php if($_GET['get']=='lvt'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(5)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(5) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" );
	
  });
</script>
<?php } else if($_GET['get']=='laminate'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(3)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(3) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" ); 
  });
</script>
<?php } else if($_GET['get']=='hardwood'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(2)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(2) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" );
  });
</script>
<?php } else if($_GET['get']=='tile'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(4)" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-labels .fl-tabs-label:nth-child(1)" ).removeClass( "fl-tab-active" );	
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(4) .fl-tabs-panel-content" ).addClass( "fl-tab-active" );
	jQuery( ".fl-tabs-panels .fl-tabs-panel:nth-child(1) .fl-tabs-panel-content" ).removeClass( "fl-tab-active" ); 
  });
</script>

<?php } ?>


	<script>
		var $menu = jQuery('.fl-page-nav-search form, header.fl-page-header-primary');

		jQuery(document).mouseup(function (e) {			
			if($(window).width() < 768){
				if (!$menu.is(e.target) && $menu.has(e.target).length === 0 && jQuery('.fl-page-nav-search form').css('display') == 'block') {
					jQuery('.fl-page-nav-search form').slideToggle();
					$('.fl-page-header-logo').toggleClass('unactiveLogo');
					$('.fl-page-nav-wrap').toggleClass('hide');
				}
			}
		});
	</script>
</body>

</html>