<div class="product-extra-info">
    <div class="row">
        <div class="col-md-12">
               <?php

                if(is_singular( 'laminate' )){
                    echo do_shortcode("[insert page='4553' display='all']");
                } 
                else if(is_singular( 'hardwood' )){
                    echo do_shortcode("[insert page='4551' display='all']");
                }
                else if(is_singular( 'carpeting' )){
                    echo do_shortcode("[insert page='4544' display='all']");
                }
                else if(is_singular( 'vinyl' )){
                    echo do_shortcode("[insert page='4555' display='all']");
                }else if(is_singular( 'floor_tile' )){
                   echo do_shortcode("[insert page='4557' display='all']");
                }

            ?>
        </div>
    </div>
</div>